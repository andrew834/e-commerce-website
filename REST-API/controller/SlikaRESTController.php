<?php

require_once("model/SlikaDB.php");
require_once("ViewHelper.php");

class SlikaRESTController {

    public static function get($id) {
        try {
            echo ViewHelper::renderJSON(SlikaDB::get(["id" => $id]));
        } catch (InvalidArgumentException $e) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"No such Slika with given ID");
            echo ViewHelper::renderJSON($data, 404);
        }
    }
    
    public static function add($data) {
        try {
            SlikaDB::insert($data);
            $data = array("message"=>"Successfull.");
            echo ViewHelper::renderJSON($data, 201);
        } catch (Exception $ex) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Error inserting slika.");
            echo ViewHelper::renderJSON($data, 400);
        }     
    }
    
    public static function edit($data) {
        try {
            SlikaDB::update($data);
            $data = array("message"=>"Successfull.");
            echo ViewHelper::renderJSON($data, 200);
        } catch (Exception $ex) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Missing data.");
            echo ViewHelper::renderJSON($data, 400); 
        }
    }
    
    public static function delete($id) {
        try {
            SlikaDB::delete(["id" => $id]);
            $data = array("message"=>"Successfull.");
            echo ViewHelper::renderJSON($data, 200);
        } catch (Exception $ex) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"No Stranka with given id.");
            echo ViewHelper::renderJSON($data, 400);
        }
    }
    
    public static function getAllByArtikelId($id){
        try {
            echo ViewHelper::renderJSON(SlikaDB::getAllByArtikelID($id));
        } catch (InvalidArgumentException $e) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Empty");
            echo ViewHelper::renderJSON($data, 404);
        }
    }
    
    public static function getFirstPic($id){
        try {
            echo ViewHelper::renderJSON(SlikaDB::getFirsPic($id));
        } catch (InvalidArgumentException $e) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Empty");
            echo ViewHelper::renderJSON($data, 404);
        }
    }
}