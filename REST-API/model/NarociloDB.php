<?php

require_once 'model/AbstractDB.php';

class NarociloDB extends AbstractDB {

    public static function insert(array $params) {
        // stranka da v bazo
        return parent::modify("INSERT INTO Narocilo (status, datum_oddano, skupnaCena, Stranka_id)"
                        . " VALUES ('waiting', :datum_oddano, :skupnaCena, :Stranka_id)", $params);
    }

    public static function update(array $params) {
        // prodajalec posodobi
        return parent::modify("UPDATE Narocilo SET status = :status, datum_odobreno = :datum_odobreno, "
                        . "Prodajalec_id = :Prodajalec_id"
                        . " WHERE id = :id", $params);
    }

    public static function delete(array $id) {
        // nesmemo jih brisati
        throw new Exception();
    }

    public static function get(array $id) {
        // podrobne informacije o narocilu z nekim idjem
        // VSAJ EN ARRTIKEL MORA BITI KUPLJEN => VRACA ARRAY
        return parent::query("SELECT n.id, n.status, n.datum_oddano, n.datum_odobreno, n.skupnaCena, s.ime, s.priimek, a.naziv, an.quantity, n.Prodajalec_id, a.cena"
                        . " FROM Narocilo n, Stranka s, Artikel_has_Narocilo an, Artikel a"
                        . " WHERE s.id = n.Stranka_id AND n.id = an.Narocilo_id AND an.Artikel_id = a.id AND n.id = :id", $id);
    }

    public static function getAll() {
        // namenjeno za prodajalca da pridobi vsa narocila
        return parent::query("SELECT n.id, n.status, n.datum_oddano, n.datum_odobreno, n.skupnaCena, s.ime, s.priimek, n.Prodajalec_id"
                . " FROM Narocilo n, Stranka s"
                . " WHERE s.id = n.Stranka_id"
                . " ORDER BY n.id ASC");
    }
    
    public static function getAllForStranka($id) {
        return parent::query("SELECT n.id, n.status, n.datum_oddano, n.datum_odobreno, n.skupnaCena, s.ime, s.priimek"
                . " FROM Narocilo n, Stranka s"
                . " WHERE s.id = n.Stranka_id AND s.id = :id", $id);
    }
    
    public static function insertArtikel_has_Narocilo(array $params) {
        // stranka da v bazo
        $a = $params["Artikel_id"];
        $b = $params["Narocilo_id"];
        $c = $params["quantity"];
        return parent::modify("INSERT INTO Artikel_has_Narocilo (Artikel_id, Narocilo_id, quantity)"
                        . " VALUES ($a, $b, $c)");
    }
}
