<?php

require_once 'model/AbstractDB.php';

class SlikaDB extends AbstractDB {

    public static function insert(array $params) {
        return parent::modify("INSERT INTO Slika (Artikel_id, slika)"
                        . " VALUES (:Artikel_id, :slika)", $params);
    }

    public static function update(array $params) {
        return parent::modify("UPDATE Slika SET Artikel_id = :Artikel_id, slika = :slika"
                        . " WHERE id = :id", $params);
    }

    public static function delete(array $id) {
        return parent::modify("DELETE FROM Slika WHERE id = :id", $id);
    }

    public static function get(array $id) {
        $slika = parent::query("SELECT id, Artikel_id, slika"
                        . " FROM Slika"
                        . " WHERE id = :id", $id);

        if (count($slika) == 1) {
            return $slika[0];
        } else {
            throw new InvalidArgumentException();
        }
    }
    
    public static function getAllByArtikelID(array $id) {
        $slike = parent::query("SELECT s.id, s.Artikel_id, s.slika"
                        . " FROM Slika s, Artikel a"
                        . " WHERE a.id = :id AND a.id = s.Artikel_id", $id);

        return $slike;
    }

    public static function getAll() {
        throw new Exception();
    }
    
    public static function getFirsPic($data){
        $slika = parent::query("SELECT s.Artikel_id, s.slika"
                    . " FROM Slika s, Artikel a"
                    . " WHERE a.id = :id AND a.id = s.Artikel_id"
                    . " LIMIT 1", $data);
        
        if (count($slika) == 1) {
            return $slika[0];
        } else {
            throw new InvalidArgumentException();
        }
    }
}
