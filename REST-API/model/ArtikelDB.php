<?php

require_once 'model/AbstractDB.php';

class ArtikelDB extends AbstractDB {

    public static function insert(array $params) {
        return parent::modify("INSERT INTO Artikel (naziv, firma, cena, opis, kategorija, statusArtikla)"
                        . " VALUES (:naziv, :firma, :cena, :opis, :kategorija, :statusArtikla)", $params);
    }

    public static function update(array $params) {
        return parent::modify("UPDATE Artikel SET naziv = :naziv, firma = :firma, "
                        . "cena = :cena, opis = :opis, kategorija = :kategorija, "
                        . "statusArtikla = :statusArtikla"
                        . " WHERE id = :id", $params);
    }

    public static function delete(array $id) {
        return parent::modify("DELETE FROM Artikel WHERE id = :id", $id);
    }

    public static function get(array $id) {
        $artikel = parent::query("SELECT id, naziv, firma, cena, opis, kategorija, statusArtikla"
                        . " FROM Artikel"
                        . " WHERE id = :id", $id);

        if (count($artikel) == 1) {
            return $artikel[0];
        } else {
            throw new InvalidArgumentException();
        }
    }

    public static function getAll() {
        return parent::query("SELECT id, naziv, firma, cena, opis, kategorija, statusArtikla"
                        . " FROM Artikel"
                        . " ORDER BY id ASC");
    }
    
    public static function getAllActive($data) {
        return parent::query("SELECT id, naziv, firma, cena, opis, kategorija, statusArtikla"
                        . " FROM Artikel WHERE statusArtikla = 'active'"
                        . " ORDER BY id ASC"
                        . " LIMIT 6 OFFSET " . $data["offset"]);
    }
}
