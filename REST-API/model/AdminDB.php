<?php

require_once 'model/AbstractDB.php';

class AdminDB extends AbstractDB {

    public static function insert(array $params) {
        throw new InvalidArgumentException();
    }

    public static function update(array $params) {
        return parent::modify("UPDATE Administrator SET ime = :ime, priimek = :priimek, "
                        . "email = :email, geslo = :geslo"
                        . " WHERE id = :id", $params);
    }

    public static function delete(array $id) {
        throw new InvalidArgumentException();
    }

    public static function get(array $id) {
        $stranke = parent::query("SELECT id, ime, priimek, email, geslo"
                        . " FROM Administrator"
                        . " WHERE id = :id", $id);

        if (count($stranke) == 1) {
            return $stranke[0];
        } else {
            throw new InvalidArgumentException();
        }
    }
    
    public static function getByEmail(array $email) {
        $admin = parent::query("SELECT id, email, geslo"
                        . " FROM Administrator"
                        . " WHERE email = :email", $email);

        if (count($admin) == 1) {
            return $admin[0];
        } else {
            throw new InvalidArgumentException();
        }
    }

    public static function getAll() {
        throw new InvalidArgumentException();
    }
}
