<?php

require_once 'model/AbstractDB.php';

class ProdajalecDB extends AbstractDB {

    public static function insert(array $params) {
        return parent::modify("INSERT INTO Prodajalec (ime, priimek, email, geslo, statusRacuna) "
                        . " VALUES (:ime, :priimek, :email, :geslo, :statusRacuna)", $params);
    }

    public static function update(array $params) {
        return parent::modify("UPDATE Prodajalec SET ime = :ime, priimek = :priimek, "
                        . "email = :email, geslo = :geslo, "
                        . "statusRacuna = :statusRacuna"
                        . " WHERE id = :id", $params);
    }

    public static function delete(array $id) {
        return parent::modify("DELETE FROM Prodajalec WHERE id = :id", $id);
    }

    public static function get(array $id) {
        $prodajalec = parent::query("SELECT id, ime, priimek, email, geslo, statusRacuna"
                        . " FROM Prodajalec"
                        . " WHERE id = :id", $id);

        if (count($prodajalec) == 1) {
            return $prodajalec[0];
        } else {
            throw new InvalidArgumentException();
        }
    }

    public static function getAll() {
        return parent::query("SELECT id, ime, priimek, email, geslo, statusRacuna"
                        . " FROM Prodajalec"
                        . " ORDER BY id ASC");
    }
    
    public static function getByEmail(array $email) {
        $stranka = parent::query("SELECT id, email, geslo"
                        . " FROM Prodajalec"
                        . " WHERE email = :email AND statusRacuna = 'active'", $email);

        if (count($stranka) == 1) {
            return $stranka[0];
        } else {
            throw new InvalidArgumentException();
        }
    }
    
    public static function updateAccountInformation(array $info) {
        return parent::modify("UPDATE Prodajalec SET ime = :ime, priimek = :priimek,"
                    . " email = :email"
                    . " WHERE id = :id", $info);
    }
    
    public static function updatePassword(array $info) {
        return parent::modify("UPDATE Prodajalec SET geslo = :geslo"
                    . " WHERE id = :id", $info);
    }
}
