<?php

require_once("controller/StrankaRESTController.php");
require_once("controller/ProdajalecRESTController.php");
require_once("controller/AdminRESTController.php");
require_once("controller/ArtikelRESTController.php");
require_once("controller/SlikaRESTController.php");
require_once("controller/OcenaRESTController.php");
require_once("controller/NarociloRESTController.php");

require_once 'checkFields.php';

$path = isset($_SERVER["PATH_INFO"]) ? trim($_SERVER["PATH_INFO"], "/") : "";

$urls = [
    "/^api\/stranka\/(\d+)$/" => function ($method, $id) {
        switch ($method) {
            case "PUT":
                $contentRaw = file_get_contents("php://input");
                $contents = utf8_encode($contentRaw);
                                
                $results = json_decode($contents, true); 

                if(is_null($results)){
                    parse_str($contentRaw,$results);
                }
                
                $data = Checker::CheckStrankaFields($results);
                
                if(!is_null($data)){
                    $data["id"] = $id[1];
                    StrankaRESTController::edit($data);
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid fields");
                    echo ViewHelper::renderJSON($data, 400);
                }
                break;
            case "DELETE":
                StrankaRESTController::delete($id[1]);
                break;
            default: # GET
                StrankaRESTController::get($id[1]);
                break;
        }
    },
    "/^api\/stranka$/" => function ($method, $id) {
        switch ($method) {
            case "POST":             
                $contentRaw = file_get_contents("php://input");
                $contents = utf8_encode($contentRaw);
                                
                $results = json_decode($contents, true); 

                if(is_null($results)){
                    parse_str($contentRaw,$results);
                }
                
                $data = Checker::CheckStrankaFields($results);
                if(!is_null($data)){
                    StrankaRESTController::add($data);
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid fields");
                    echo ViewHelper::renderJSON($data, 400);
                }
                
                break;
            default: # GET
                StrankaRESTController::getAll();
                break;
        }
    },
    "/^api\/stranka\/validate$/" => function ($method, $id) {
        switch ($method) {
            case "POST":             
                $contentRaw = file_get_contents("php://input");
                $contents = utf8_encode($contentRaw);
                                
                $results = json_decode($contents, true); 

                if(is_null($results)){
                    parse_str($contentRaw,$results);
                }
                
                if(!is_null($results)){   
                    StrankaRESTController::getByEmailAndVerify($results);
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid login");
                    echo ViewHelper::renderJSON($data, 400);
                }
                
                break;
            default: 
                $data = array("error" => "There was an error", "status"=>404, "message"=>"Unknown path");
                echo ViewHelper::renderJSON($data, 404);
                break;
        }
    },
    "/^api\/stranka\/account\/(\d+)$/" => function ($method, $id) {
        switch ($method) {
            case "PUT":        // ta posodobi informacije o racunu      
                $contentRaw = file_get_contents("php://input");
                $contents = utf8_encode($contentRaw);
                                
                $results = json_decode($contents, true); 

                if(is_null($results)){
                    parse_str($contentRaw,$results);
                }
                
                $data = Checker::CheckStrankaInfo($results);
                
                if(!is_null($data)){
                    $data["id"] = $id[1];
                    StrankaRESTController::updateAccountInformation($data);
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid data");
                    echo ViewHelper::renderJSON($data, 400);
                }
                
                break;
            case "POST":    // ta posodobi gelso
                $contents = file_get_contents("php://input");
                $contents = utf8_encode($contents);
                $results = json_decode($contents, true); 
                
                if(is_null($results)){
                    $results = $_POST;
                }
                
                $data = Checker::CheckStrankaPassword($results);
                if(!is_null($data)){
                    $data["id"] = $id[1];
                    StrankaRESTController::updatePassword($data);
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid data");
                    echo ViewHelper::renderJSON($data, 400);
                }
                break;
            default: 
                $data = array("error" => "There was an error", "status"=>404, "message"=>"Unknown path");
                echo ViewHelper::renderJSON($data, 404);
                break;
        }
    },
    "/^api\/prodajalec\/(\d+)$/" => function ($method, $id) {
        switch ($method) {
            case "PUT":
                $contentRaw = file_get_contents("php://input");
                $contents = utf8_encode($contentRaw);
                                
                $results = json_decode($contents, true); 

                if(is_null($results)){
                    parse_str($contentRaw,$results);
                }
                
                $data = Checker::CheckProdajalecFields($results);
                
                if(!is_null($data)){
                    $data["id"] = $id[1];
                    ProdajalecRESTController::edit($data);
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid fields");
                    echo ViewHelper::renderJSON($data, 400);
                }
                break;
            case "DELETE":
                ProdajalecRESTController::delete($id[1]);
                break;
            default: # GET
                ProdajalecRESTController::get($id[1]);
                break;
        }
    },
    "/^api\/prodajalec$/" => function ($method, $id) {
        switch ($method) {
            case "POST":             
                $contentRaw = file_get_contents("php://input");
                $contents = utf8_encode($contentRaw);
                                
                $results = json_decode($contents, true); 

                if(is_null($results)){
                    parse_str($contentRaw,$results);
                }
                
                $data = Checker::CheckProdajalecFields($results);
                
                if(!is_null($data)){
                    ProdajalecRESTController::add($data);
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid fields");
                    echo ViewHelper::renderJSON($data, 400);
                }
                
                break;
            default: # GET
                ProdajalecRESTController::getAll();
                break;
        }
    },
    "/^api\/prodajalec\/validate$/" => function ($method, $id) {
        switch ($method) {
            case "POST":             
                $contentRaw = file_get_contents("php://input");
                $contents = utf8_encode($contentRaw);
                                
                $results = json_decode($contents, true); 

                if(is_null($results)){
                    parse_str($contentRaw,$results);
                }
                
                if(!is_null($results)){
                    ProdajalecRESTController::getByEmailAndVerify($results);
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid login");
                    echo ViewHelper::renderJSON($data, 400);
                }
                
                break;
            default: 
                $data = array("error" => "There was an error", "status"=>404, "message"=>"Unknown path");
                echo ViewHelper::renderJSON($data, 404);
                break;
        }
    },
    "/^api\/prodajalec\/account\/(\d+)$/" => function ($method, $id) {
        switch ($method) {
            case "PUT":        // ta posodobi informacije o racunu      
                $contentRaw = file_get_contents("php://input");
                $contents = utf8_encode($contentRaw);
                                
                $results = json_decode($contents, true); 

                if(is_null($results)){
                    parse_str($contentRaw,$results);
                }
                
                $data = Checker::CheckProdajalecInfo($results);

                
                if(!is_null($data)){
                    $data["id"] = $id[1];
                    ProdajalecRESTController::updateAccountInformation($data);
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid data");
                    echo ViewHelper::renderJSON($data, 400);
                }
                
                break;
            case "POST":    // ta posodobi gelso
                $contents = file_get_contents("php://input");
                $contents = utf8_encode($contents);
                $results = json_decode($contents, true); 
                
                if(is_null($results)){
                    $results = $_POST;
                }
                
                $data = Checker::CheckProdajalecPassword($results);
                if(!is_null($data)){
                    $data["id"] = $id[1];
                    ProdajalecRESTController::updatePassword($data);
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid data");
                    echo ViewHelper::renderJSON($data, 400);
                }
                break;
            default: 
                $data = array("error" => "There was an error", "status"=>404, "message"=>"Unknown path");
                echo ViewHelper::renderJSON($data, 404);
                break;
        }
    },       
    "/^api\/admin\/validate$/" => function ($method, $id) {
        switch ($method) {
            case "POST":             
                $contentRaw = file_get_contents("php://input");
                $contents = utf8_encode($contentRaw);
                                
                $results = json_decode($contents, true); 

                if(is_null($results)){
                    parse_str($contentRaw,$results);
                } 
                
                if(!is_null($results)){
                    AdminRESTController::getByEmailAndVerify($results);
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid login");
                    echo ViewHelper::renderJSON($data, 400);
                }
                
                break;
            default: 
                $data = array("error" => "There was an error", "status"=>404, "message"=>"Unknown path");
                echo ViewHelper::renderJSON($data, 404);
                break;
        }
    },
    "/^api\/admin\/(\d+)$/" => function ($method, $id) {
        switch ($method) {
            case "GET":             
                $results = AdminRESTController::get($id);
                break;
            case "PUT":
                $contentRaw = file_get_contents("php://input");
                $contents = utf8_encode($contentRaw);
                
                $results = json_decode($contents, true); 
               
                if(is_null($results)){
                    parse_str($contentRaw,$results);
                }
                
                $results = Checker::CheckAdminFields($results);

                if(!is_null($results)){
                    $results["id"] = $id[1];
                    AdminRESTController::edit($results);
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid fields.");
                    echo ViewHelper::renderJSON($data, 400);
                }
                
                break;
            default:
                $data = array("error" => "There was an error", "status"=>404, "message"=>"Unknown path");
                echo ViewHelper::renderJSON($data, 404);
                break;
        }
    },
    "/^api\/artikel\/(\d+)$/" => function ($method, $id) {
        switch ($method) {
            case "PUT":
                $contentRaw = file_get_contents("php://input");
                $contents = utf8_encode($contentRaw);
                                
                $results = json_decode($contents, true); 

                if(is_null($results)){
                    parse_str($contentRaw,$results);
                }
                
                $data = Checker::CheckArtikelFields($results);
                
                if(!is_null($data)){
                    $data["id"] = $id[1];
                    ArtikelRESTController::edit($data);
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid fields");
                    echo ViewHelper::renderJSON($data, 400);
                }
                break;
            case "DELETE":
                ArtikelRESTController::delete($id[1]);
                break;
            default: # GET
                ArtikelRESTController::get($id[1]);
                break;
        }
    },
    "/^api\/artikel$/" => function ($method, $id) {
        switch ($method) {
            case "POST":             
                $contentRaw = file_get_contents("php://input");
                $contents = utf8_encode($contentRaw);
                                
                $results = json_decode($contents, true); 

                if(is_null($results)){
                    parse_str($contentRaw,$results);
                }
                
                $data = Checker::CheckArtikelFields($results);
                
                if(!is_null($data)){
                    ArtikelRESTController::add($data);
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid fields");
                    echo ViewHelper::renderJSON($data, 400);
                }
                
                break;
            default: # GET
                ArtikelRESTController::getAll();
                break;
        }
    },
    "/^api\/artikel-stranka\/(\d+)$/" => function ($method, $id) {
        ArtikelRESTController::getAllActive(array("offset" => $id[1]));
    },
    "/^api\/slika\/(\d+)$/" => function ($method, $id) {
        switch ($method) {
            case "PUT":
                $contentRaw = file_get_contents("php://input");
                //$contents = utf8_encode($contentRaw);
                                
                $results = json_decode($contentRaw, true); 

                if(is_null($results)){
                    parse_str($contentRaw,$results);
                }
                
                if(Checker::CheckSlikaFields($results)){
                    $results["id"] = $id[1];
                    SlikaRESTController::edit($results);
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid fields");
                    echo ViewHelper::renderJSON($data, 400);
                }
                break;
            case "DELETE":
                SlikaRESTController::delete($id[1]);
                break;
            default: # GET
                SlikaRESTController::get($id[1]);
                break;
        }
    },
    "/^api\/slika$/" => function ($method, $id) {
        switch ($method) {
            case "POST":             
                $contentRaw = file_get_contents("php://input");
                //$contents = utf8_encode($contentRaw);            
                $results = json_decode($contentRaw, true); 

                if(is_null($results)){
                    parse_str($contentRaw,$results);
                }
                
                if(Checker::CheckSlikaFields($results)){
                    SlikaRESTController::add($results);
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid fields");
                    echo ViewHelper::renderJSON($data, 400);
                }            
                break;
            default: 
                $data = array("error" => "There was an error", "status"=>404, "message"=>"Unknown path");
                echo ViewHelper::renderJSON($data, 404);
                break;
        }
    },
    "/^api\/slika-artikel\/(\d+)$/" => function ($method, $id) {
        SlikaRESTController::getAllByArtikelId(array("id" => $id[1]));
    },
    "/^api\/slika-artikel-first\/(\d+)$/" => function ($method, $id) {
        SlikaRESTController::getFirstPic(array("id" => $id[1]));
    },
    "/^api\/ocena\/(\d+)$/" => function ($method, $id) {
        OcenaRESTController::getAllOcenaByArtikelID(array("Artikel_id" => $id[1]));
    },
    "/^api\/ocena$/" => function ($method, $id) {
        switch ($method) {
            case "POST":             
                $contentRaw = file_get_contents("php://input");
                $contents = utf8_encode($contentRaw);            
                $results = json_decode($contentRaw, true); 

                if(is_null($results)){
                    parse_str($contentRaw,$results);
                }

                $results = Checker::CheckOcenaFields($results);
                
                if(!is_null($results)){
                    OcenaRESTController::add($results);
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid fields");
                    echo ViewHelper::renderJSON($data, 400);
                }            
                break;
            default: 
                $data = array("error" => "There was an error", "status"=>404, "message"=>"Unknown path");
                echo ViewHelper::renderJSON($data, 404);
                break;
        }
    },
    "/^api\/narocilo\/(\d+)$/" => function ($method, $id) {
        // podrobni podatki narocila VRACA ARRAY
        switch ($method) {
            case "GET":
                NarociloRESTController::get($id[1]);
                break;
            case "PUT":
                $contentRaw = file_get_contents("php://input");
                $contents = utf8_encode($contentRaw);            
                $results = json_decode($contentRaw, true); 

                if(is_null($results)){
                    parse_str($contentRaw,$results);
                }
                
                $results = Checker::CheckProdajalecInput($results);
                
                if(!is_null($results)) {
                    NarociloRESTController::edit(array("status" => $results["status"], "datum_odobreno" => $results["datum_odobreno"],
                                                       "Prodajalec_id" => $results["Prodajalec_id"], "id" => $id[1]));
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid fields");
                    echo ViewHelper::renderJSON($data, 400);
                    exit();
                }
                
                break;
            default:
                $data = array("error" => "There was an error", "status"=>404, "message"=>"Unknown path");
                echo ViewHelper::renderJSON($data, 404);
                break;
        }
    },
    "/^api\/narocilo$/" => function ($method, $id) {
        switch ($method) {
            case "POST":             
                $contentRaw = file_get_contents("php://input");
                $contents = utf8_encode($contentRaw);            
                $results = json_decode($contentRaw, true); 

                if(is_null($results)){
                    parse_str($contentRaw,$results);
                }
    
                $results = Checker::CheckNarociloFields($results);
                
                if(!is_null($results)){
                    $narociloID = NarociloRESTController::add($results);
                    foreach ($results["izdelki"] as $key => $value) {
                        NarociloRESTController::insertArtikel_has_Narocilo(array("Artikel_id" => $value['Artikel_id'], "Narocilo_id" => $narociloID, "quantity" => $value['quantity']));
                    }
                }else{
                    $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid fields");
                    echo ViewHelper::renderJSON($data, 400);
                    exit();
                }     
                
                $data = array("message"=>"Successfull.");
                echo ViewHelper::renderJSON($data, 201);
                
                break;
             case "GET":
                // namenjeno za prodajalca da pridobi vsa narocila
                NarociloRESTController::getAll();
                break;
            default: 
                $data = array("error" => "There was an error", "status"=>404, "message"=>"Unknown path");
                echo ViewHelper::renderJSON($data, 404);
                break;
        }
    },
    "/^api\/narocilo-stranka\/(\d+)$/" => function ($method, $id) {
        // podrobni podatki narocila VRACA ARRAY
        switch ($method) {
            case "GET":
                NarociloRESTController::getAllForStranka($id[1]);
                break;
            default:
                $data = array("error" => "There was an error", "status"=>404, "message"=>"Unknown path");
                echo ViewHelper::renderJSON($data, 404);
                break;
        }
    }
];

$c = 0;

foreach ($urls as $pattern => $controller) {
    if (preg_match($pattern, $path, $params)) {
        $c = 1;
        try {
            $controller($_SERVER["REQUEST_METHOD"], $params);
        } catch (InvalidArgumentException $e) {
            ViewHelper::error404();
        } catch (Exception $e) {
            ViewHelper::displayError($e, true);
        }
    }
}

if($c != 1){
    $data = array("error" => "There was an error", "status"=>404, "message"=>"Unknown path");
    echo ViewHelper::renderJSON($data, 404);
}

?>