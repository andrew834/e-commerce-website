<?php
    class Checker{    
        public static function checkCert(array $authorized_users){
            $client_cert = filter_input(INPUT_SERVER, "SSL_CLIENT_CERT");
            
            if ($client_cert == null) {
                return false;//die('err: Spremenljivka SSL_CLIENT_CERT ni nastavljena.');
            }

            $cert_data = openssl_x509_parse($client_cert);
            $commonname = (is_array($cert_data['subject']['CN']) ?
                            $cert_data['subject']['CN'][0] : $cert_data['subject']['CN']);
            
            //var_dump($authorized_users);
            
            if (in_array($commonname, $authorized_users)) {
                return true;
            } else {
                return false;
            }
        }
        
        public static function myRole(){
            $client_cert = filter_input(INPUT_SERVER, "SSL_CLIENT_CERT");
            
            if ($client_cert == null) {
                return NULL;
            }

            $cert_data = openssl_x509_parse($client_cert);
            $commonname = (is_array($cert_data['subject']['CN']) ?
                            $cert_data['subject']['CN'][0] : $cert_data['subject']['CN']);
            
            return $commonname;
        
        }    
    }    
?>