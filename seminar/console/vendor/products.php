<?php

    session_start();
    
    if(!isset($_SESSION["id"])){
        $newURL= str_replace("/console/vendor/products.php","/login.php", $_SERVER["REQUEST_URI"]);
        header('Location: '.$newURL);
        die();
    }

    if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
            $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            header('Location: ' . $redirect);
            exit();
        }

        require_once '../../checkCerts.php';

        $role = Checker::myRole();

        //ce je ROLE NULL ali razlicen od Prodajalec potem mu ne dovolimo dostopa
        if(is_null($role) || $role !== "Prodajalec"){
            $newURL= str_replace("/console/vendor/products.php","/login.php", $_SERVER["REQUEST_URI"]);
            header('Location: '.$newURL);
            die();
        }

        // si admin in lahko dostopas do console
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/artikel");
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json'
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = json_decode(curl_exec($ch), true);

        curl_close($ch);
    
    ?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Products</title>
      <link rel="stylesheet" type="text/css" href="../../css/admin-console.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../assets/css/Navigation-Clean.css">
   </head>
   <body>
      <div>
         <nav class="navbar navbar-default navigation-clean">
            <div class="container">
               <div class="navbar-header">
                  <button class="navbar-toggle collapsed menu-button" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                  <p class="navbar-brand">Online shop</p>
               </div>
               <div class="collapse navbar-collapse" id="navcol-1">
                  <ul class="nav navbar-nav navbar-right">
                     <li role="presentation"><a href="<?=str_replace("/console/vendor/products.php", "/shop/main.php", $_SERVER["PHP_SELF"])?>">Shop</a></li>
                     <li role="presentation"><a href="<?=str_replace("/products.php", "/customer.php", $_SERVER["PHP_SELF"])?>">Shopper console</a></li>
                     <li role="presentation"><a href="<?=$_SERVER["PHP_SELF"]?>">Products</a></li>
                     <li role="presentation"><a href="<?=str_replace("/products.php", "/orders.php", $_SERVER["PHP_SELF"])?>">Orders</a></li>
                     <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Settings <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                           <li role="presentation"><a href="<?=str_replace("/console/vendor/products.php", "/account/settings.php", $_SERVER["PHP_SELF"])?>">Account</a></li>
                           <li role="presentation"><a href="<?=str_replace("/console/vendor/products.php", "/logout.php", $_SERVER["PHP_SELF"])?>">Logout</a></li>
                        </ul>
                     </li>
                  </ul>
               </div>
            </div>
         </nav>
      </div>
      <div class="bar"></div>
              <div class="container" style="margin-bottom: 20px;">
            <div class="row row-fix">
               <div class="col-md-12">
                  <h1 class="bolded h1-titles">Products</h1>
               </div>
            </div>
        </div>
      <?php
        if(empty($output)){
            ?>
                <div class="container" style="margin-top:10px;">
                   <div class="row row-fix">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 details">
                          <p class="input-b" name="location">Empty</p>
                      </div>
                   </div>
                </div>
      <?php
        }else{
            foreach($output as $key=>$value){
                $who = $value["naziv"] . " | " . $value["firma"] . " | " . $value["statusArtikla"];
                ?>
                <div class="container" style="margin-top:10px;">
                    <div class="row row-fix">
                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 details">
                           <p class="input-b" name="location"><?=$who?></p>
                          <a href="<?=str_replace("/products.php","/edit-product.php?id=" . $value["id"], $_SERVER["PHP_SELF"])?>"><button class="edit2"><i class="glyphicon glyphicon-pencil"></i></button></a>
                       </div>
                    </div>
                </div>
                <?php
            }
        }  
      ?>
      <div class="text-center" style="margin-top: 40px;">
         <a href="<?=str_replace("/products.php","/new-product.php", $_SERVER["PHP_SELF"])?>"><button class="btn btn-primary" class="btn-post">New product</button></a>
      </div>
      <script src="../../assets/js/jquery.min.js"></script>
      <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
   </body>
</html>