<?php

    session_start();
    
    if(!isset($_SESSION["id"])){
        $newURL= str_replace("/console/vendor/edit-product.php","/login.php", $_SERVER["REQUEST_URI"]);
        header('Location: '.$newURL);
        die();
    }

    if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('Location: ' . $redirect);
        exit();
    }
    
    require_once '../../checkCerts.php';
    
    $role = Checker::myRole();
    
    //ce je ROLE NULL ali razlicen od prodajalca potem mu ne dovolimo dostopa
    if(is_null($role) || $role !== "Prodajalec"){
        $newURL= str_replace("/console/vendor/edit-product.php","/login.php", $_SERVER["REQUEST_URI"]);
        header('Location: '.$newURL);
        die();
    }
    $id = 0;
    $wasPost = false;
    $change = false;
    
    if(isset($_POST["id"])){
        $id = $_POST["id"];
        $wasPost = true;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/artikel/" . $id);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_POST));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = json_decode(curl_exec($ch), true);
        //$output = curl_exec($ch);
        curl_close($ch);
        
        if(isset($output["message"]) && strcmp($output["message"], "Successfull.") == 0){
            $change = true;
        }
        
    }else if(!isset($_GET["id"])){
        die();
    }else{
        $id = $_GET["id"];
    }
    
    // si admin in lahko dostopas do console
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/artikel/" . $id);
    $headers = array(
        'Accept: application/json',
        'Content-Type: application/json'
    );

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = json_decode(curl_exec($ch), true);
    curl_close($ch);
    
    if(isset($output["error"])){
        var_dump($output);
        die();
    }
    
    $naziv = $output["naziv"];
    $firma = $output["firma"];
    $cena = $output["cena"];
    $opis = $output["opis"];
    $kategorija = $output["kategorija"];
    $statusArtikla = $output["statusArtikla"];
    $edit = str_replace("/edit-product.php", "/edit-product-pictures.php" . "?id=" . $id, $_SERVER["PHP_SELF"]);
    $postLocation = $_SERVER["PHP_SELF"] . "?id=" . $id;
    
    ?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Edit product</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../assets/css/Navigation-Clean.css">
      <link rel="stylesheet" type="text/css" href="../../css/profileSettings.css">
   </head>
   <body>
      <div>
         <nav class="navbar navbar-default navigation-clean">
            <div class="container">
               <div class="navbar-header">
                  <button class="navbar-toggle collapsed menu-button" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                  <p class="navbar-brand">Online shop</p>
               </div>
               <div class="collapse navbar-collapse" id="navcol-1">
                  <ul class="nav navbar-nav navbar-right">
                     <li role="presentation"><a href="<?=str_replace("/console/vendor/edit-product.php", "/shop/main.php", $_SERVER["PHP_SELF"])?>">Shop</a></li>
                     <li role="presentation"><a href="<?=str_replace("/edit-product.php", "/customer.php", $_SERVER["PHP_SELF"])?>">Shopper console</a></li>
                     <li role="presentation"><a href="<?=str_replace("/edit-product.php", "/products.php", $_SERVER["PHP_SELF"])?>">Products</a></li>
                     <li role="presentation"><a href="<?=str_replace("/edit-product.php", "/orders.php", $_SERVER["PHP_SELF"])?>">Orders</a></li>
                     <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Settings <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                           <li role="presentation"><a href="<?=str_replace("/console/vendor/edit-product.php", "/account/settings.php", $_SERVER["PHP_SELF"])?>">Account</a></li>
                           <li role="presentation"><a href="<?=str_replace("/console/vendor/edit-product.php", "/logout.php", $_SERVER["PHP_SELF"])?>">Logout</a></li>
                        </ul>
                     </li>
                  </ul>
               </div>
            </div>
         </nav>
      </div>
      <div class="bar"></div>
      <div class="container">
         <h2 class="h2-name"><?=$naziv?></h2>
         <img src="../../images/Dropbox-icon.png" id="profileSettingsAvatar" alt="Product_icon.png">
         <div class="text-center">
            <a href="<?=$edit?>"><button class="btn btn-primary" class="btn-post">Edit product pictures</button></a>
         </div>
         <?php
            if($wasPost){
                if($change){ ?>
                  <div class="alert alert-success alert-dismissible show" role="alert" style="margin-top:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Success, </strong>the data was changed.
                  </div> <?php
                }else{ ?>
                  <div class="alert alert-danger alert-dismissible show" role="alert" style="margin-top:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Oh snap, </strong>please check your input.
                  </div> 
                <?php    
                }
            }
         ?>
         
        <form action="<?=$postLocation?>" method="post">
           <input type="hidden" name="id" value="<?=$id?>">
           <div class="form-group">
              <label for="naziv">Name</label>
              <input class="form-control" minlength="3" maxlength="255" name="naziv" type="text" placeholder="Name" value="<?=$naziv?>" required>
           </div>
           <div class="form-group">
              <label for="firma">Brand</label>
              <input class="form-control" minlength="3" maxlength="255" name="firma" type="text" placeholder="Brand" value="<?=$firma?>" required>
           </div>
           <div class="form-group">
              <label for="cena">Price</label>
              <input class="form-control" minlength="3" maxlength="255" placeholder="Set price: 40.23" name="cena" type="text" value="<?=$cena?>" required>
           </div>
           <div class="form-group">
              <label for="opis">Description</label>
              <input class="form-control" minlength="3" maxlength="1024" placeholder="Description" name="opis" type="text" value="<?=$opis?>">
           </div>
           <div class="form-group">
              <label for="kategorija">Category</label>
              <input class="form-control" minlength="3" maxlength="255" placeholder="Category" name="kategorija" type="text" value="<?=$kategorija?>">
           </div>
           <div class="form-group">
            <label for="statusArtikla">Product status</label>
            <select class="form-control" name="statusArtikla">
              <option>active</option>
              <option>disabled</option>
            </select>
           </div> 
           <div class="text-center">
              <button class="btn btn-primary" type="submit" class="btn-post">Edit product</button>
           </div>
        </form>
      </div>
      <script src="../../assets/js/jquery.min.js"></script>
      <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
   </body>
</html>