<?php

    session_start();
    
    if(!isset($_SESSION["id"])){
        $newURL= str_replace("/console/admin/new-vendor.php","/login.php", $_SERVER["REQUEST_URI"]);
        header('Location: '.$newURL);
        die();
    }

    if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('Location: ' . $redirect);
        exit();
    }

    require_once '../../checkCerts.php';
    
    $role = Checker::myRole();
    
    //ce je ROLE NULL ali razlicen od administrator potem mu ne dovolimo dostopa
    if(is_null($role) || $role !== "Administrator"){
        $newURL= str_replace("/console/admin/new-vendor.php","/login.php", $_SERVER["REQUEST_URI"]);
        header('Location: '.$newURL);
        die();
    }

    $wasPost = false;
    $change = false;
    
    if(isset($_POST["newVendor"])){
        $wasPost = true;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/prodajalec/");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_POST));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = json_decode(curl_exec($ch), true);
        
        curl_close($ch);

        if(isset($output["message"]) && strcmp($output["message"], "Successfull.") == 0){
            $change = true;
        }
    }
    
    $postLocation = $_SERVER["PHP_SELF"];
 ?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>New vendor</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../assets/css/Navigation-Clean.css">
      <link rel="stylesheet" type="text/css" href="../../css/profileSettings.css">
   </head>
   <body>
      <div>
         <nav class="navbar navbar-default navigation-clean">
            <div class="container">
               <div class="navbar-header">
                  <button class="navbar-toggle collapsed menu-button" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                  <p class="navbar-brand">Online shop</p>
               </div>
               <div class="collapse navbar-collapse" id="navcol-1">
                  <ul class="nav navbar-nav navbar-right">
                     <li role="presentation"><a href="<?=str_replace("/console/admin/new-vendor.php", "/shop/main.php", $_SERVER["PHP_SELF"])?>">Shop</a></li>
                     <li role="presentation"><a href="<?=str_replace("/admin/new-vendor.php", "/admin.php", $_SERVER["PHP_SELF"])?>">Admin console</a></li>
                     <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Settings <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                           <li role="presentation"><a href="<?=str_replace("/console/admin/new-vendor.php", "/account/settings.php", $_SERVER["PHP_SELF"])?>">Account</a></li>
                           <li role="presentation"><a href="<?=str_replace("/console/admin/new-vendor.php", "/logout.php", $_SERVER["PHP_SELF"])?>">Logout</a></li>
                        </ul>
                     </li>
                  </ul>
               </div>
            </div>
         </nav>
      </div>
      <div class="bar"></div>
      <div class="container">
         <h2 class="h2-name">New vendor</h2>
         <img src="https://www.w3schools.com/howto/img_avatar.png" id="profileSettingsAvatar" alt="Avatar_Icon.png">
         <?php
            if($wasPost){
                if($change){ ?>
                  <div class="alert alert-success alert-dismissible show" role="alert" style="margin-top:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Success, </strong>the data was changed.
                  </div> <?php
                }else{ ?>
                  <div class="alert alert-danger alert-dismissible show" role="alert" style="margin-top:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Oh snap, </strong>please check your input.
                  </div> 
                <?php    
                }
            }
         ?>
         
         <form action="<?=$postLocation?>" method="post">
            <input type="hidden" name="newVendor" value="newVendor">
            <div class="form-group">
               <label for="ime">Name</label>
               <input class="form-control" minlength="3" maxlength="255" name="ime" type="text" placeholder="Name" value="" required>
            </div>
            <div class="form-group">
               <label for="priimek">Surname</label>
               <input class="form-control" minlength="3" maxlength="255" name="priimek" type="text" placeholder="Surname" value="" required>
            </div>
            <div class="form-group">
               <label for="email">Email</label>
               <input class="form-control" minlength="3" maxlength="255" placeholder="Set email" name="email" type="email" value="" required>
            </div>
            <div class="form-group">
               <label for="geslo">New password</label>
               <input class="form-control" name="geslo" minlength="6" maxlength="255" type="password" placeholder="Password" value="" required>
            </div>
            <div class="form-group">
               <label for="geslo_repeat">Confirm password</label>
               <input class="form-control" name="geslo_repeat" minlength="6" maxlength="255" type="password" placeholder="Password confirmation" value="" required>
            </div>
            <div class="form-group">
             <label for="statusRacuna">Account status</label>
             <select class="form-control" name="statusRacuna">
               <option>active</option>
               <option>disabled</option>
             </select>
            </div> 
            <div class="text-center">
               <button class="btn btn-primary" type="submit" class="btn-post">Create account</button>
            </div>
         </form>
      </div>
      <script src="../../assets/js/jquery.min.js"></script>
      <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
   </body>
</html>
