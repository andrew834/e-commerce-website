function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function addToCookie(obj) {
	var productID = obj.parentNode.querySelector(".idArtikla").innerText.trim();
	var productTitle = obj.parentNode.querySelector("h2").innerText.trim();
	var productPrice = obj.parentNode.querySelector(".price").innerText.trim();
	console.log("Izdelek = " + productTitle + " price = " + productPrice);
	var c = getCookie(createCookieData(productID, productTitle, productPrice));
	if (c.trim().length == 0) {
		setCookie(createCookieData(productID, productTitle, productPrice), 1, 30);
	} else {
		setCookie(createCookieData(productID, productTitle, productPrice), parseInt(c) + 1, 30);
	}

	/*var str = "<tr>"
	str += "<th scope='row'> 1</th>"; //TODO: ST IZDELKA #
	str += "<td class='productTitle'> " + productTitle + " </td>";
	str += "<td><input class='number_of_items' type='text' id='number_of_items;" + productTitle + "' onchange='updateValue(this)' myvalue='1' value='1'></td>"
	str += "<td class='cenaIzdelka'>800€</td>";
	str += "<td><button type='button' onclick='removeFromModalCart(this)' class='removeButton btn'><span class='glyphicon glyphicon-trash'></span></button></td>";
	str += "</tr>"
	*/

	/*var updateInputList = document.querySelectorAll(".number_of_items");

	console.log(updateInputList);

	for (var i = 0; i < updateInputList.length; i++) {
		console.log(updateInputList[i].id.split(";")[1].trim());
		console.log(productTitle.trim());
		if (updateInputList[i].id.split(";")[1].trim() == productTitle.trim()) {
			updateInputList[i].value = parseInt(updateInputList[i].getAttribute("myvalue")) + 1;
			updateInputList[i].setAttribute("value", updateInputList[i].value);
			updateInputList[i].setAttribute("myvalue", updateInputList[i].value);
			return;
		}
		updateInputList[i].value = updateInputList[i].getAttribute("myvalue");
	}
	document.getElementById("modaltablebody").innerHTML += str;*/

}

function generateTableFromCookie() {
	document.getElementById("modaltablebody").innerHTML = "";
	var cookiesArr = document.cookie.split(";");
	if (cookiesArr.length == 1 && cookiesArr[0].length == 0) {
		return;
	}

	var products = {};
	for (let i = 0; i < cookiesArr.length; i++) {
		var cArr = cookiesArr[i].split("=");
		products[cArr[0]] = cArr[1];
	}

	console.log(products);

	var str = "<tr>"
        var counter = 1;
	for (var key in products) {
		if (key.trim() == "oddanoDne" || key.trim() == "PHPSESSID") {
			continue;
		}

		var splitArray = key.split("#");
		var idIzdelka = splitArray[0];
		var izdelek = splitArray[1];
		var cena = splitArray[2];
                console.log(cena);
		var kolicina = products[key];
		var splitCena = cena.split(" ");



		str += "<th scope='row'>" + counter+ "</th>";
		str += "<td class='productTitle'> " + izdelek + " </td>";
		str += "<td><input class='number_of_items cartInfo' type='text' id='number_of_items;" + izdelek + "' idizdelka='" + idIzdelka + "'onchange='updateValue(this)' myvalue='" + kolicina + "' value='" + kolicina + "'></td>"
		str += "<td class='cenaIzdelka'>" + cena + "</td>";
		str += "<td><button type='button' onclick='removeFromModalCart(this)' class='removeButton btn'><span class='glyphicon glyphicon-trash'></span></button></td>";
		str += "</tr>"
                counter++;
	}

	document.getElementById("modaltablebody").innerHTML += str;


}

function containsAlready(izdelek) {
	var arr = document.querySelectorAll(".productTitle");
	for (var i = 0; i < arr.length; i++) {
		console.log(arr[i].innerText);
		console.log(izdelek);
		if (arr[i].innerText.trim() == izdelek.trim()) {
			return true;
		}
	}
	return false;
}

function createCookieData(productID, izdelek, cena) {
	return productID + "#" + izdelek + "#" + cena;
}

function deleteAllItems() {
	document.getElementById("modaltablebody").innerHTML = "";
	var cookiesArr = document.cookie.split(";");
	for (var i = 0; i < cookiesArr.length; i++) {
		var c = cookiesArr[i].split("=");
		if (c[0] == "PHPSESSID") {
			continue;
		}
		deleteCookie(c[0]);
	}
}

function updateValue(obj) {
	var c = obj.value;
	document.getElementById(obj.id).value = c;
	document.getElementById(obj.id).setAttribute("myvalue", c);
	var productTitle = obj.id.split(";")[1];
	var cenaIzdelka = parseFloat(obj.parentNode.parentNode.querySelector(".cenaIzdelka").innerText);
	var idIzdelka = obj.getAttribute("idizdelka");
	setCookie(createCookieData(idIzdelka, productTitle.trim(), cenaIzdelka + " €"), c, 30);
}

function formatDateNumber(n) {
	if (n < 10) {
		return "0" + n;
	}
	return n;
}

function confirmButton() {
	console.log("haha!");
	var d = new Date();
	var leto = formatDateNumber(d.getFullYear());
	var mesec = formatDateNumber(d.getMonth() + 1);
	var dan = formatDateNumber(d.getDate());
	document.querySelector(".trenutniDatum").innerHTML = "Confirmation date: " + dan + "/" + mesec + "/" + leto;
	//document.getElementById("pripraviRacun").innerText = "Send";
	var cookiesArr = document.cookie.split(";");

	var lastLines = "<tr><td class='thick-line'></td>";
	lastLines += "<td class='thick-line'></td>";
	lastLines += "<td class='thick-line text-center'><strong>Total</strong></td>";
	document.getElementById("racunBody").innerHTML = "";

	var products = {};

	if (cookiesArr.length == 1 && cookiesArr[0].length == 0) {
		return;
	}
	for (let i = 0; i < cookiesArr.length; i++) {
		var cArr = cookiesArr[i].split("=");
		products[cArr[0]] = cArr[1];
	}

	console.log(products);
	var s = 0;
	var str = "";
	for (var key in products) {
		if (key.trim() == "oddanoDne" || key.trim() == "PHPSESSID") {
			continue;
		}
                str += "<tr>";
		var splitArray = key.split("#");
		var izdelek = splitArray[1];
		var cena = splitArray[2];
		var kolicina = products[key];
		var splitCena = cena.split(" ");
		var izracunanaCena = parseFloat(splitCena[0]) * parseInt(kolicina);
		str += "<td>" + izdelek + "</td>";
		str += "<td class='text-center'>" + cena + "</td>";
		str += "<td class='text-center'>" + kolicina + "</td>";
		str += "<td class='text-right'>" + izracunanaCena + " €</td>";
		s += izracunanaCena;
                str += "</tr>";
	}

	lastLines += "<td class='thick-line text-right'>" + s + " €</td>";
	str += lastLines;
	document.getElementById("racunBody").innerHTML += str;

	setCookie("oddanoDne", +dan + "#" + mesec + "#" + leto);
	$("sendButton").css("display", "block");
	$("confirmButton").css("display", "none");
	$("#kartModal").modal('hide');
	$("#racunModal").modal('show');
}

function sendButton() {
    var loc = location.origin;
    loc+="/netbeans/seminar/shop/order.php";
    location.href = loc;
}

function removeFromModalCart(obj) {
	var productTitle = obj.parentNode.parentNode.querySelector(".productTitle").innerText;
	deleteCookie(productTitle.trim());
	console.log("removing..");
	obj.parentNode.parentNode.remove();
}

function deleteCookie(name) {
	setCookie(name, '', -1);
}