<?php
    session_start();
    $anonimenodjemalec = !isset($_SESSION["id"]); 
    
    // si prijavljen ampak nimas HTTPS
    if((empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") && !$anonimenodjemalec){
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('Location: ' . $redirect);
        exit();
    }

    require_once '../checkCerts.php';
    
    $role = Checker::myRole();

    $id = 1;
    
    if(isset($_GET["id"]) && is_int((int)$_GET["id"])){
        $id = (int)$_GET["id"];
    }
    
    if(isset($_POST["ocena"])){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/ocena");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_POST));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = json_decode(curl_exec($ch), true);
        
        curl_close($ch);
        
        if(isset($output["message"]) && strcmp($output["message"], "Successfull.") == 0){
            $change = true;
        }
    }
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/artikel/" . $id);
    $headers = array(
        'Accept: application/json',
        'Content-Type: application/json'
    );

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = json_decode(curl_exec($ch), true);
    
    curl_close($ch);
    
    if(is_null($output) || isset($output["error"])){
        die();
    }
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/ocena/" . $output["id"]);
    $headers = array(
        'Accept: application/json',
        'Content-Type: application/json'
    );

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $outputOcena = json_decode(curl_exec($ch), true);
    curl_close($ch);
    $ocena = 0;
    $numberOfOcena = 0;
    
    if(isset($outputOcena["error"]) || empty($outputOcena)){
        $numberOfOcena = 1;
    }
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/slika-artikel/" . $id);
    $headers = array(
        'Accept: application/json',
        'Content-Type: application/json'
    );

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $outputSlika = json_decode(curl_exec($ch), true);

    curl_close($ch);
    
    
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Shop</title>
      <link rel="stylesheet" type="text/css" href="../css/details.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" href="../assets/css/Navigation-Clean.css">
      <!-- Font Awesome Icon Library -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   </head>
   <body>


    <?php if($role === "Stranka" && !$anonimenodjemalec){ ?>
             <div>
                <nav class="navbar navbar-default navigation-clean">
                   <div class="container">
                      <div class="navbar-header">
                         <button class="navbar-toggle collapsed menu-button" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                         <p class="navbar-brand">Online shop</p>
                      </div>
                      <div class="collapse navbar-collapse" id="navcol-1">
                         <ul class="nav navbar-nav navbar-right">
                            <li role="presentation"><a href="#kartModal" id="cart" onclick="generateTableFromCookie()" data-toggle="modal"><i class="fa fa-shopping-cart"></i> Cart </a></li>
                            <li role="presentation"><a href="<?=str_replace("/details.php","/main.php", $_SERVER["PHP_SELF"])?>">Shop</a></li>
                            <li role="presentation"><a href="<?=str_replace("/shop/details.php", "/console/customer/orders.php", $_SERVER["PHP_SELF"])?>">Orders</a></li>
                            <li class="dropdown">
                               <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Settings <span class="caret"></span></a>
                               <ul class="dropdown-menu" role="menu">
                                  <li role="presentation"><a href="<?=str_replace("/shop/details.php","/account/settings.php", $_SERVER["PHP_SELF"])?>">Account</a></li>
                                  <li role="presentation"><a href="<?=str_replace("/shop/details.php", "/logout.php", $_SERVER["PHP_SELF"])?>">Logout</a></li>
                               </ul>
                            </li>
                         </ul>
                      </div>
                   </div>
                </nav>
             </div>
            <!-- Modal -->
            <div class="modal fade" id="kartModal" tabindex="-1" role="dialog">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><b>My Cart</b></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                     </div>
                     <div class="modal-body">
                        <form>
                           <table class="table table-bordered">
                              <thead>
                                 <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Item</th>
                                    <th scope="col">Number of items</th>
                                    <th scope="col">Price</th>
                                    <th scope="col"></th>
                                 </tr>
                              </thead>
                              <tbody id="modaltablebody">
                              </tbody>
                           </table>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-success" style="float:center;" data-dismiss="modal" id="confirmButton" onclick="confirmButton()">Confirm</button>
                        <button type="button" class="btn btn-danger" id="deletAllItems" style="float: left; padding-top:5px; padding-bottom:5px;" onclick="deleteAllItems()"><span  style="float: left; padding-top:5px; padding-bottom:5px;" class='glyphicon glyphicon-trash'></button>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="racunModal" tabindex="-1" role="dialog">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">My cart</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                     </div>
                     <div class="modal-body">
                        <form>
                           <h3 class="trenutniDatum"> 01/02/2009</h3>
                           <div class="panel panel-default" style="margin-top: 25px;">
                              <div class="panel-heading">
                                 <h3 class="panel-title"><strong>Cart confirmation</strong></h3>
                                 <div class="panel-body">
                                    <div class="table-responsive">
                                       <table class="table table-condensed">
                                          <thead>
                                             <tr>
                                                <td><strong>Item</strong></td>
                                                <td class="text-center"><strong>Price</strong></td>
                                                <td class="text-center"><strong>Quantity</strong></td>
                                                <td class="text-right"><strong>Totals</strong></td>
                                             </tr>
                                          </thead>
                                          <tbody id="racunBody">
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal" id="sendButton" onclick="sendButton()">Send</button>
                        <button type="button" class="btn btn-secondary" style="float: left;" data-dismiss="modal">Close</button>
                     </div>
                  </div>
               </div>
            </div>
    <?php }else if($role === "Administrator" && !$anonimenodjemalec){ ?>
             <div>
                <nav class="navbar navbar-default navigation-clean">
                   <div class="container">
                      <div class="navbar-header">
                         <button class="navbar-toggle collapsed menu-button" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                         <p class="navbar-brand">Online shop</p>
                      </div>
                      <div class="collapse navbar-collapse" id="navcol-1">
                         <ul class="nav navbar-nav navbar-right">
                            <li role="presentation"><a href="<?=str_replace("/details.php","/main.php", $_SERVER["PHP_SELF"])?>">Shop</a></li>
                            <li role="presentation"><a href="<?=str_replace("/shop/details.php","/console/admin.php", $_SERVER["PHP_SELF"])?>">Admin console</a></li>
                            <li class="dropdown">
                               <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Settings <span class="caret"></span></a>
                               <ul class="dropdown-menu" role="menu">
                                  <li role="presentation"><a href="<?=str_replace("/shop/details.php","/account/settings.php", $_SERVER["PHP_SELF"])?>">Account</a></li>
                                  <li role="presentation"><a href="<?=str_replace("/shop/details.php", "/logout.php", $_SERVER["PHP_SELF"])?>">Logout</a></li>
                               </ul>
                            </li>
                         </ul>
                      </div>
                   </div>
                </nav>
             </div>
    <?php }else if($role === "Prodajalec" && !$anonimenodjemalec){ ?>
             <div>
                <nav class="navbar navbar-default navigation-clean">
                   <div class="container">
                      <div class="navbar-header">
                         <button class="navbar-toggle collapsed menu-button" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                         <p class="navbar-brand">Online shop</p>
                      </div>
                      <div class="collapse navbar-collapse" id="navcol-1">
                         <ul class="nav navbar-nav navbar-right">
                            <li role="presentation"><a href="<?=str_replace("/details.php", "/main.php", $_SERVER["PHP_SELF"])?>">Shop</a></li>
                            <li role="presentation"><a href="<?=str_replace("/shop/details.php", "/console/vendor/customer.php", $_SERVER["PHP_SELF"])?>">Shopper console</a></li>
                            <li role="presentation"><a href="<?=str_replace("/shop/details.php", "/console/vendor/products.php", $_SERVER["PHP_SELF"])?>">Products</a></li>
                            <li role="presentation"><a href="<?=str_replace("/shop/details.php", "/console/vendor/orders.php", $_SERVER["PHP_SELF"])?>">Orders</a></li>
                            <li class="dropdown">
                               <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Settings <span class="caret"></span></a>
                               <ul class="dropdown-menu" role="menu">
                                  <li role="presentation"><a href="<?=str_replace("/shop/details.php","/account/settings.php", $_SERVER["PHP_SELF"])?>">Account</a></li>
                                  <li role="presentation"><a href="<?=str_replace("/shop/details.php", "/logout.php", $_SERVER["PHP_SELF"])?>">Logout</a></li>
                               </ul>
                            </li>
                         </ul>
                      </div>
                   </div>
                </nav>
             </div>
    <?php }else{ ?>
             <div>
                <nav class="navbar navbar-default navigation-clean">
                   <div class="container">
                      <div class="navbar-header">
                         <button class="navbar-toggle collapsed menu-button" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                         <p class="navbar-brand">Online shop</p>
                      </div>
                      <div class="collapse navbar-collapse" id="navcol-1">
                         <ul class="nav navbar-nav navbar-right">
                            <li role="presentation"><a href="<?=str_replace("/details.php", "/main.php", $_SERVER["PHP_SELF"])?>">Shop</a></li>
                            <li role="presentation"><a href="<?=str_replace("/shop/details.php", "/register.php", $_SERVER["PHP_SELF"])?>">Register</a></li>
                         </ul>
                      </div>
                   </div>
                </nav>
             </div> 
    <?php } ?>
     
      <div class="bar"></div>
      <!-- Modal -->
      <div class="modal fade" id="kartModal" tabindex="-1" role="dialog">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">My cart</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <form>
                     <table class="table table-bordered">
                        <thead>
                           <tr>
                              <th scope="col">#</th>
                              <th scope="col">Item</th>
                              <th scope="col">Number of items</th>
                              <th scope="col">Price</th>
                              <th scope="col"></th>
                           </tr>
                        </thead>
                        <tbody id="modaltablebody">
                        </tbody>
                     </table>
                  </form>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-success" style="float:center;" data-dismiss="modal" id="confirmButton" onclick="confirmButton()">Confirm</button>
                  <button type="button" class="btn btn-danger" id="deletAllItems" style="float: left; padding-top:5px; padding-bottom:5px;" onclick="deleteAllItems()"><span  style="float: left; padding-top:5px; padding-bottom:5px;" class='glyphicon glyphicon-trash'></button>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="racunModal" tabindex="-1" role="dialog">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">My cart</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <form>
                     <h3 class="trenutniDatum"> 01/02/2009</h3>
                     <div class="panel panel-default" style="margin-top: 25px;">
                        <div class="panel-heading">
                           <h3 class="panel-title"><strong>Cart confirmation</strong></h3>
                           <div class="panel-body">
                              <div class="table-responsive">
                                 <table class="table table-condensed">
                                    <thead>
                                       <tr>
                                          <td><strong>Item</strong></td>
                                          <td class="text-center"><strong>Price</strong></td>
                                          <td class="text-center"><strong>Quantity</strong></td>
                                          <td class="text-right"><strong>Totals</strong></td>
                                       </tr>
                                    </thead>
                                    <tbody id="racunBody">
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-success" data-dismiss="modal" id="sendButton" onclick="sendButton()">Send</button>
                  <button type="button" class="btn btn-secondary" style="float: left;" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <div class="container" id="details"  style="border: 3px solid #7a7d82; border-radius: 10px; box-shadow: 3px 2px #abaeb2;">
         <div class="row">
            <div class="col-md-5">
               <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner"> <?php
                    if(isset($outputSlika["error"]) || empty($outputSlika)){ ?>
                     <div class="item active">
                        <img src="../images/Dropbox-icon.png" alt="Product pic">
                     </div>
                     <div class="item">
                        <img src="../images/Dropbox-icon.png" alt="Product pic">
                     </div>
                    <?php }else{
                      $counter = 0;
                      foreach ($outputSlika as $keySlika => $slikaValue){ ?>
                        <div class="item <?php if($counter == 0){ ?> active <?php } ?>">
                           <img src="<?=$slikaValue["slika"]?>" alt="Product pic" style="height: 460px;" width="500px">
                        </div>                   
                
                <?php $counter = $counter + 1; }   
                    }
                ?></div>
                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  <span class="sr-only">Next</span>
                  </a>
               </div>
            </div>
                <?php foreach ($outputOcena as $ocenaKey => $ocenaValue) {
                          $ocena = $ocena + $ocenaValue["ocena"];
                          $numberOfOcena = $numberOfOcena + 1;
                        }
                    $ocena = (int)($ocena/(int)$numberOfOcena);
                ?>
             
            <div class="col-md-3" style="margin-top: 10px;">
               <p class="newarrival text-center"><b>NEW</b></p>
               <h2><?=$output["naziv"]?></h2>
               <span class="idArtikla" style="display:none"><?=$output["id"]?></span>
                <?php
                    for ($i = 1; $i <= $ocena; $i++) { ?>
                        <span class="fa fa-star checked"></span>
                <?php } ?>
                <?php
                    for ($i = $ocena; $i < 5; $i++) { ?>
                        <span class="fa fa-star"></span>
                <?php } ?>
               <p class="price"><?=$output["cena"]?> €</p>
               <p><b>Availability: </b>YES</p>
               <p><b>Condition: </b> TOP </p>
               <p><b>Brand: </b><?=$output["firma"]?> </p>
               <button type="button" onclick="addToCookie(this)" class="btn btn-default cart"> Add to cart </button>
            </div>
            <div class="col-md-4" style="margin-top: 10px;">
               <div class="form-group">
                  <label for="description">Description</label>
                  <textarea class="form-control" id="description" readonly><?=$output["opis"]?></textarea>
               </div>
            </div>
         </div>
      </div>
      <div class="container">
         <div class="row">
            <h3>Comments: </h3>
         </div>
      </div>
      <?php foreach($outputOcena as $ocenaKey => $ocenaValue){ ?>
         <div class="container comments">
            <div class="row">
               <div class="allcomments2">
                    <?php
                        for ($i = 1; $i <= $ocenaValue["ocena"]; $i++) { ?>
                            <span class="fa fa-star checked"></span>
                    <?php } ?>
                    <?php
                        for ($i = $ocenaValue["ocena"]; $i < 5; $i++) { ?>
                            <span class="fa fa-star"></span>
                    <?php } ?>
                  <p><b><?=$ocenaValue["ime"]?></b> <?=$ocenaValue["opis"]?></p>
               </div>
            </div>
         </div>  
      <?php } ?>
      <?php
      if($role === "Stranka"){ ?>
           <form method="POST" action="<?=$_SERVER["PHP_SELF"] . "?id=" . $id?>">
             <input type="hidden" name="Artikel_id" value="<?=$id?>">
             <input type="hidden" name="Stranka_id" value="<?=$_SESSION["id"]?>">
             <div class="container">
                <div class="row" >
                   <div class="col-sm-7">
                      <textarea class="form-control usercomment" name="opis" placeholder="Type your comment ..."></textarea>
                   </div>
                   <div class="col-sm-2">
                      <div class="rating">
                         <span onclick="starClicked(5)" id="z5">☆</span><span onclick="starClicked(4)"  id="z4">☆</span><span onclick="starClicked(3)"  id="z3">☆</span><span onclick="starClicked(2)"  id="z2">☆</span><span onclick="starClicked(1)"  id="z1">☆</span>
                      </div>
                   </div>
                </div>
             </div>
             <input type="hidden" name="ocena" id="stZvezdic" value="0">
             <div class="container">
                <div class="row" style="padding-bottom: 200px !important;">
                   <div class="col-sm-3">
                      <button type="submit" id="submitbutton"class="btn btn-primary">Submit</button>
                   </div>
                </div>
             </div>
          </form>
        <?php } ?>

       
    	<script src="../assets/js/jquery.min.js"></script>
	<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="../assets/js/stars.js"></script>
        
      <?php if($role === "Stranka"){ ?>
            <script src="../assets/js/shoppingCart.js"></script>
      <?php } ?>
    </body>
</html>