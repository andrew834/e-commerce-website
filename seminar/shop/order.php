<?php

    session_start();

    $anonimenodjemalec = !isset($_SESSION["id"]); 
    require_once '../checkCerts.php';
    
    $role = Checker::myRole();
    
    if($anonimenodjemalec || $role !== "Stranka" || count($_COOKIE) == 2){
        $redirect = str_replace("/shop/order.php", "/shop/main.php", $_SERVER["PHP_SELF"]);
        header('Location: ' . $redirect);
        exit();
    }
    
    // si prijavljen ampak nimas HTTPS
    if((empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off")){
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('Location: ' . $redirect);
        exit();
    }
    
    $date = date("d/m/Y");
    $izdelki = array();
    $c = 0;
    $skupnaCena = 0;
 
    var_dump(count($_COOKIE));
    
    $narociloPOSLJI = array();
    $narociloPOSLJI["datum_oddano"] = $date;
    $narociloPOSLJI["Stranka_id"] = $_SESSION["id"];
    $narociloPOSLJI["skupnaCena"] = 0; //se posodobi kasneje
    $narociloPOSLJI["izdelki"] = array();
    
    foreach ($_COOKIE as $key => $value) {
        if($key === "PHPSESSID" || $key === "oddanoDne"){
            continue;
        }
        
        $array = explode("#", $key);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/artikel/" . $array[0]);
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json'
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = json_decode(curl_exec($ch), true);

        if(empty($output) || isset($output["message"]) || !isset($output["id"])){
            echo("Something went wrong");
            die();
        }
        
        curl_close($ch);
    
        $skupnaCena = $skupnaCena + $output["cena"] * $value;
        $item = array("Artikel_id" => $output["id"], "quantity" => $value);    
        $narociloPOSLJI["izdelki"][$c] = $item;
        
        $c = $c+1;  
    }
    // pripravljeno za zapis v bazo
    $narociloPOSLJI["skupnaCena"] = $skupnaCena;
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/narocilo");
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($narociloPOSLJI));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    $output = json_decode(curl_exec($ch), true);
    
    curl_close($ch);

    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . str_replace("/shop/order.php", "/shop/main.php", $_SERVER['REQUEST_URI']);
    header('Location: ' . $redirect);
    
    