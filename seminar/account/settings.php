<?php

    session_start();
    
    if(!isset($_SESSION["id"])){
        $newURL= str_replace("/account/settings.php","/login.php", $_SERVER["REQUEST_URI"]);
        header('Location: '.$newURL);
        die();
    }

    if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('Location: ' . $redirect);
        exit();
    }

    require_once '../checkCerts.php';
    
    $role = Checker::myRole();
    
    //ce je ROLE NULL potem ni autoriziran
    if(is_null($role)){
        $newURL= str_replace("/account/settings.php","/login.php", $_SERVER["REQUEST_URI"]);
        header('Location: '.$newURL);
        die();
    }
    
    if(strcmp($role, "Stranka") == 0){
        // samo ce je oseba prijavljena
        $id = $_SESSION["id"]; // DOBIMO IS SESSIONA
        $change = false;
        $wasPost = false;
        if(isset($_POST['chaneInformation'])){
            $wasPost = true;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/stranka/account/" . $id);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_POST));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $output = json_decode(curl_exec($ch), true);

            curl_close($ch);

            if(isset($output["message"]) && strcmp($output["message"], "Successfull.") == 0){
                $change = true;
            }
        }else if(isset($_POST['changePassword'])){
            $wasPost = true;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/stranka/account/" . $id);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_POST));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $output = json_decode(curl_exec($ch), true);

            curl_close($ch);

            if(isset($output["message"]) && strcmp($output["message"], "Successfull.") == 0){
                $change = true;
            }
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/stranka/" . $id);
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json'
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = json_decode(curl_exec($ch), true);

        curl_close($ch);

        $ime = $output["ime"];
        $priimek = $output["priimek"];
        $email = $output["email"];
        $telefon = $output["telefon"];
        $naslov = $output["naslov"];
        $who = $ime . " " . $priimek;
    ?>
    <!DOCTYPE html>
    <html lang="en">
       <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <meta http-equiv="X-UA-Compatible" content="ie=edge">
          <title>Settings</title>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
          <link rel="stylesheet" href="../assets/css/Navigation-Clean.css">
          <link rel="stylesheet" type="text/css" href="../css/profileSettings.css">
       </head>
       <body>
          <div>
             <nav class="navbar navbar-default navigation-clean">
                <div class="container">
                   <div class="navbar-header">
                      <button class="navbar-toggle collapsed menu-button" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                      <p class="navbar-brand">Online shop</p>
                   </div>
                   <div class="collapse navbar-collapse" id="navcol-1">
                      <ul class="nav navbar-nav navbar-right">
                         <li role="presentation"><a href="<?=str_replace("/account/settings.php", "/shop/main.php", $_SERVER["PHP_SELF"])?>">Shop</a></li>
                         <li role="presentation"><a href="<?=str_replace("/account/settings.php", "/console/customer/orders.php", $_SERVER["PHP_SELF"])?>">Orders</a></li>
                         <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Settings <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                               <li role="presentation"><a href="<?=$_SERVER["PHP_SELF"]?>">Account</a></li>
                               <li role="presentation"><a href="<?=str_replace("/account/settings.php", "/logout.php", $_SERVER["PHP_SELF"])?>">Logout</a></li>
                            </ul>
                         </li>
                      </ul>
                   </div>
                </div>
             </nav>
          </div>
          <div class="bar"></div>
          <div class="container">
             <h2 class="h2-name"><?=$who?></h2>
             <img src="https://www.w3schools.com/howto/img_avatar.png" id="profileSettingsAvatar" alt="Avatar_Icon.png">
             <?php
                if($wasPost){
                    if($change){ ?>
                      <div class="alert alert-success alert-dismissible show" role="alert" style="margin-top:20px;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Success, </strong>your data was changed.
                      </div> <?php
                    }else{ ?>
                      <div class="alert alert-danger alert-dismissible show" role="alert" style="margin-top:20px;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Oh snap, </strong>please check your input.
                      </div> 
                    <?php    
                    }
                }
             ?>
             <form action="<?=$_SERVER["PHP_SELF"]?>" method="post">
                <input type="hidden" name="chaneInformation" value="chaneInformation">
                <div class="form-group">
                   <label for="ime">Name</label>
                   <input class="form-control" minlength="3" maxlength="255" name="ime" type="text" placeholder="Change name" value="<?=$ime?>" required>
                </div>
                <div class="form-group">
                   <label for="nickname">Surname</label>
                   <input class="form-control" minlength="3" maxlength="255" name="priimek" type="text" placeholder="Change surname" value="<?=$priimek?>" required>
                </div>
                <div class="form-group">
                   <label for="email">Email</label>
                   <input class="form-control" minlength="3" maxlength="255" type="email" value="<?=$email?>" disabled>
                </div>
                <div class="form-group">
                   <label for="telefon">Phone</label>
                   <input class="form-control" name="telefon" type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{3}" placeholder="Phone: 123-456-789" value="<?=$telefon?>" required>
                </div>
                <div class="form-group">
                   <label for="naslov">Address</label>
                   <input class="form-control" minlength="3" maxlength="255" name="naslov" type="text" placeholder="Change address" value="<?=$naslov?>" required>
                </div>
                <div class="text-center">
                   <button class="btn btn-primary" type="submit" class="btn-post">Update information</button>
                </div>
             </form>
          </div>
              <div class="container">
             <h2 class="h2-name" style="margin-top:100px;">Change password</h2>
             <form action="<?=$_SERVER["PHP_SELF"]?>" method="post">
                <input type="hidden" name="changePassword" value="changePassword">
                <div class="form-group">
                   <label for="staro_geslo">Old password</label>
                   <input class="form-control" name="staro_geslo" minlength="6" maxlength="255" type="password" placeholder="Old password" value="" required>
                </div>
                <div class="form-group">
                   <label for="novo_geslo">New password</label>
                   <input class="form-control" name="novo_geslo" minlength="6" maxlength="255" type="password" placeholder="New password" value="" required>
                </div>
                <div class="form-group">
                   <label for="novo_geslo_repeat">Confirm password</label>
                   <input class="form-control" name="novo_geslo_repeat" minlength="6" maxlength="255" type="password" placeholder="Password confirmation" value="" required>
                </div>
                <div class="text-center">
                   <button class="btn btn-primary" type="submit" class="btn-post">Update password</button>
                </div>
             </form>
          </div>
          <script src="../assets/js/jquery.min.js"></script>
          <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
       </body>
    </html>

    <?php
    }else if(strcmp($role, "Administrator") == 0){
        // samo ce je oseba prijavljena
        $id = $_SESSION["id"]; // DOBIMO IS SESSIONA
        $change = false;
        $wasPost = false;
        
        if(isset($_POST['chaneInformation'])){
            $wasPost = true;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/admin/" . $id);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_POST));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $output = json_decode(curl_exec($ch), true);
            curl_close($ch);

            if(isset($output["message"]) && strcmp($output["message"], "Successfull.") == 0){
                $change = true;
            }
        }
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/admin/" . $id);
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json'
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = json_decode(curl_exec($ch), true);

        curl_close($ch);

        $ime = $output["ime"];
        $priimek = $output["priimek"];
        $email = $output["email"];
        $who = $ime . " " . $priimek;
        
        ?>
        <!DOCTYPE html>
        <html lang="en">
           <head>
              <meta charset="UTF-8">
              <meta name="viewport" content="width=device-width, initial-scale=1">
              <meta http-equiv="X-UA-Compatible" content="ie=edge">
              <title>Settings</title>
              <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
              <link rel="stylesheet" href="../assets/css/Navigation-Clean.css">
              <link rel="stylesheet" type="text/css" href="../css/profileSettings.css">
           </head>
           <body>
              <div>
                 <nav class="navbar navbar-default navigation-clean">
                    <div class="container">
                       <div class="navbar-header">
                          <button class="navbar-toggle collapsed menu-button" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                          <p class="navbar-brand">Online shop</p>
                       </div>
                       <div class="collapse navbar-collapse" id="navcol-1">
                          <ul class="nav navbar-nav navbar-right">
                             <li role="presentation"><a href="<?=str_replace("/account/settings.php", "/shop/main.php", $_SERVER["PHP_SELF"])?>">Shop</a></li>
                             <li role="presentation"><a href="<?=str_replace("/account/settings.php", "/console/admin.php", $_SERVER["PHP_SELF"])?>">Admin console</a></li>
                             <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Settings <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                   <li role="presentation"><a href="<?=$_SERVER["PHP_SELF"]?>">Account</a></li>
                                   <li role="presentation"><a href="<?=str_replace("/account/settings.php", "/logout.php", $_SERVER["PHP_SELF"])?>">Logout</a></li>
                                </ul>
                             </li>
                          </ul>
                       </div>
                    </div>
                 </nav>
              </div>
              <div class="bar"></div>
              <div class="container">
                 <h2 class="h2-name"><?=$who?></h2>
                 <img src="https://www.w3schools.com/howto/img_avatar.png" id="profileSettingsAvatar" alt="Avatar_Icon.png">
                 <?php
                    if($wasPost){
                        if($change){ ?>
                          <div class="alert alert-success alert-dismissible show" role="alert" style="margin-top:20px;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Success, </strong>your data was changed.
                          </div> <?php
                        }else{ ?>
                          <div class="alert alert-danger alert-dismissible show" role="alert" style="margin-top:20px;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Oh snap, </strong>please check your input.
                          </div> 
                        <?php    
                        }
                    }
                 ?>
                 <form action="<?=$_SERVER["PHP_SELF"]?>" method="post">
                    <input type="hidden" name="chaneInformation" value="chaneInformation">
                    <div class="form-group">
                       <label for="ime">Name</label>
                       <input class="form-control" minlength="3" maxlength="255" name="ime" type="text" placeholder="Change name" value="<?=$ime?>" required>
                    </div>
                    <div class="form-group">
                       <label for="nickname">Surname</label>
                       <input class="form-control" minlength="3" maxlength="255" name="priimek" type="text" placeholder="Change surname" value="<?=$priimek?>" required>
                    </div>
                    <div class="form-group">
                       <label for="email">Email</label>
                       <input class="form-control" minlength="3" maxlength="255" name="email" type="email" value="<?=$email?>" required>
                    </div>
                    <div class="form-group">
                       <label for="geslo">Old password</label>
                       <input class="form-control" name="geslo" minlength="6" maxlength="255" type="password" placeholder="Old password" value="" required>
                    </div>
                    <div class="form-group">
                       <label for="geslo_novo">New password</label>
                       <input class="form-control" name="geslo_novo" minlength="6" maxlength="255" type="password" placeholder="New password" value="" required>
                    </div>
                    <div class="form-group">
                       <label for="geslo_novo_repeat">Confirm password</label>
                       <input class="form-control" name="geslo_novo_repeat" minlength="6" maxlength="255" type="password" placeholder="Password confirmation" value="" required>
                    </div>
                    <div class="text-center">
                       <button class="btn btn-primary" type="submit" class="btn-post">Update account</button>
                    </div>
                 </form>
              </div>
              <script src="../assets/js/jquery.min.js"></script>
              <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
           </body>
        </html>
<?php
        }else if(strcmp($role, "Prodajalec") == 0){
                        // samo ce je oseba prijavljena
            $id = $_SESSION["id"]; // DOBIMO IS SESSIONA
            $change = false;
            $wasPost = false;
            if(isset($_POST['chaneInformation'])){
                $wasPost = true;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/prodajalec/account/" . $id);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_POST));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                $output = json_decode(curl_exec($ch), true);
                curl_close($ch);

                if(isset($output["message"]) && strcmp($output["message"], "Successfull.") == 0){
                    $change = true;
                }
            }else if(isset($_POST['changePassword'])){
                $wasPost = true;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/prodajalec/account/" . $id);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_POST));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                $output = json_decode(curl_exec($ch), true);

                curl_close($ch);

                if(isset($output["message"]) && strcmp($output["message"], "Successfull.") == 0){
                    $change = true;
                }
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/prodajalec/" . $id);
            $headers = array(
                'Accept: application/json',
                'Content-Type: application/json'
            );

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = json_decode(curl_exec($ch), true);

            curl_close($ch);

            $ime = $output["ime"];
            $priimek = $output["priimek"];
            $email = $output["email"];
            $who = $ime . " " . $priimek;
?>
    <!DOCTYPE html>
    <html lang="en">
       <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <meta http-equiv="X-UA-Compatible" content="ie=edge">
          <title>Settings</title>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
          <link rel="stylesheet" href="../assets/css/Navigation-Clean.css">
          <link rel="stylesheet" type="text/css" href="../css/profileSettings.css">
       </head>
       <body>
          <div>
             <nav class="navbar navbar-default navigation-clean">
                <div class="container">
                   <div class="navbar-header">
                      <button class="navbar-toggle collapsed menu-button" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                      <p class="navbar-brand">Online shop</p>
                   </div>
                   <div class="collapse navbar-collapse" id="navcol-1">
                      <ul class="nav navbar-nav navbar-right">
                         <li role="presentation"><a href="<?=str_replace("/account/settings.php", "/shop/main.php", $_SERVER["PHP_SELF"])?>">Shop</a></li>
                         <li role="presentation"><a href="<?=str_replace("/account/settings.php","/console/vendor/customer.php", $_SERVER["PHP_SELF"])?>">Shopper console</a></li>
                         <li role="presentation"><a href="<?=str_replace("/account/settings.php", "/console/vendor/products.php", $_SERVER["PHP_SELF"])?>">Products</a></li>
                         <li role="presentation"><a href="<?=str_replace("/account/settings.php", "/console/vendor/orders.php", $_SERVER["PHP_SELF"])?>">Orders</a></li>
                         <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Settings <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                               <li role="presentation"><a href="<?=$_SERVER["PHP_SELF"]?>">Account</a></li>
                               <li role="presentation"><a href="<?=str_replace("/account/settings.php", "/logout.php", $_SERVER["PHP_SELF"])?>">Logout</a></li>
                            </ul>
                         </li>
                      </ul>
                   </div>
                </div>
             </nav>
          </div>
          <div class="bar"></div>
          <div class="container">
             <h2 class="h2-name"><?=$who?></h2>
             <img src="https://www.w3schools.com/howto/img_avatar.png" id="profileSettingsAvatar" alt="Avatar_Icon.png">
             <?php
                if($wasPost){
                    if($change){ ?>
                      <div class="alert alert-success alert-dismissible show" role="alert" style="margin-top:20px;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Success, </strong>your data was changed.
                      </div> <?php
                    }else{ ?>
                      <div class="alert alert-danger alert-dismissible show" role="alert" style="margin-top:20px;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Oh snap, </strong>please check your input.
                      </div> 
                    <?php    
                    }
                }
             ?>
             <form action="<?=$_SERVER["PHP_SELF"]?>" method="post">
                <input type="hidden" name="chaneInformation" value="chaneInformation">
                <div class="form-group">
                   <label for="ime">Name</label>
                   <input class="form-control" minlength="3" maxlength="255" name="ime" type="text" placeholder="Change name" value="<?=$ime?>" required>
                </div>
                <div class="form-group">
                   <label for="nickname">Surname</label>
                   <input class="form-control" minlength="3" maxlength="255" name="priimek" type="text" placeholder="Change surname" value="<?=$priimek?>" required>
                </div>
                <div class="form-group">
                   <label for="email">Email</label>
                   <input class="form-control" minlength="3" maxlength="255" name="email" type="email" value="<?=$email?>" required>
                </div>
                <div class="text-center">
                   <button class="btn btn-primary" type="submit" class="btn-post">Update information</button>
                </div>
             </form>
          </div>
          <div class="container">
             <h2 class="h2-name" style="margin-top:100px;">Change password</h2>
             <form action="<?=$_SERVER["PHP_SELF"]?>" method="post">
                <input type="hidden" name="changePassword" value="changePassword">
                <div class="form-group">
                   <label for="staro_geslo">Old password</label>
                   <input class="form-control" name="staro_geslo" minlength="6" maxlength="255" type="password" placeholder="Old password" value="" required>
                </div>
                <div class="form-group">
                   <label for="novo_geslo">New password</label>
                   <input class="form-control" name="novo_geslo" minlength="6" maxlength="255" type="password" placeholder="New password" value="" required>
                </div>
                <div class="form-group">
                   <label for="novo_geslo_repeat">Confirm password</label>
                   <input class="form-control" name="novo_geslo_repeat" minlength="6" maxlength="255" type="password" placeholder="Password confirmation" value="" required>
                </div>
                <div class="text-center">
                   <button class="btn btn-primary" type="submit" class="btn-post">Update password</button>
                </div>
             </form>
          </div>
          <script src="../assets/js/jquery.min.js"></script>
          <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
       </body>
    </html>
   
   <?php
    }
   ?>